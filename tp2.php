<!DOCTYPE html>
<html>
<head>
	<style type="text/css">
		ul{
			list-style-type: square;
		}
		li{
			margin-top: 3px;
		}
		table{
			border-collapse: collapse;
			border: solid 1px ;
			margin-left: 15px;
			margin-top: 15px;
		}
		th{
			border: solid 1px ;
		}
		td{
			border: solid 1px ;
			padding-left: 5px;
			padding-right: 5px
		}
		p{
			margin-top: 5px;
			margin-bottom: 5px;
			margin-left: 15px;

		}
	</style>
	
</head>
<body>
	<h3><u>	Ex1</u></h3>
	<?php
		$str='Hello World';
		echo "<span>a) Display \$str in uppercase</span>: " .strtoupper($str);
		echo "<br><br><span>b) Display \$str in lowercase</span>: " .strtolower($str);
	?>

	<h3><u>	Ex2</u></h3>
	<?php
		$name = 'Jonh';

		#Concatenation String
		$con = "Hello"." ".$name;

		echo '<span>a) Using string concatenation display: </span>'.$con;
		echo "<br><br><span>b) Using string interpolation display: </span> $con";

	?>

	<h3><u>	Ex3</u></h3>

	<?php
		$sample ='The str_replace method is used to perform replacement in targeted string by the given search term.<br>The str_replace is case-sensitive function';

		echo str_replace("str_replace", "Replace", $sample).".";

	?>
	<h3><u>	Ex4</u></h3>
	<?php
		function check_primnum($num){
			$c=0;
			for($i=2;$i<=$num;$i++){
				if($num%$i==0){
					$c++;
				}
			}
			if($c==1){
				echo "The $num is primary number.";
			}
			else{
				echo "The $num is not primary number.";
			}
		};

		check_primnum(1);
	?>

	<h3><u>	Ex5</u></h3>
	<?php
		function reverse_string($string){
			for($i=strlen($string)-1;$i>=0;$i--){
				echo "$string[$i]";
			}
		}
		reverse_string('now');
	?>

	<h3><u>	Ex6</u></h3>
	<?php
	function check_palindrome($string) 
	{
	  if ($string == strrev($string))
	      return "it is palindrome.";
	  else
		  return "it is not palindrome.";
	}
	
	echo check_palindrome('madam');
	?>

	<h3><u>	Ex7</u></h3>

	<?php
		$days = array('Monday','Tuesday','Wednessday','Thursday','Friday','Saturday','Sunday');
		echo "a) Here are days of week: <br>";
		echo "<ul>";
		for($i=0;$i<sizeof($days);$i++){
			echo "<li>$days[$i]<br></lil>";
		}
		echo "</ul>";
		echo "<br><br>b) Underlined one is the cuurrent date:<br>";
		echo "<ul>";
		for($i=0;$i<sizeof($days);$i++){
			if($days[$i]==date("l")){
 	 			echo "<li><u>$days[$i]</u></li>";
			}
			else{
				echo "<li>$days[$i]</li>";
			}
		}
		echo "</ul>";

	?>


	<h3><u>	Ex8</u></h3>
	<?php
		$countries = array( "Italy"=>"Rome",
		 "Luxembourg"=>"Luxembourg", 
		 "Belgium"=>"Brussels",
		  "Denmark"=>"Copenhagen",
		   "Finland"=>"Helsinki",
		    "France" => "Paris",
		    "Slovakia"=>"Bratislava",
		     "Slovenia"=>"Ljubljana",
		      "Germany" => "Berlin",
		       "Greece" =>"Athens",
		        "Ireland"=>"Dublin",
		         "Netherlands"=>"Amsterdam",
		          "Portugal"=>"Lisbon",
		          "Spain"=>"Madrid",
		           "Sweden"=>"Stockholm",
		            "United Kingdom"=>"London", 
		            "Cyprus"=>"Nicosia",
		             "Lithuania"=>"Vilnius",
		              "Czech Republic"=>"Prague",
		              "Estonia"=>"Tallin",
		               "Hungary"=>"Budapest",
		                "Latvia"=>"Riga",
		                 "Malta"=>"Valetta",
		                  "Austria" => "Vienna",
		                   "Poland"=>"Warsaw");
		echo "<span>a) Sort value of array:</span>";
		asort($countries);
		echo "<table>";
		echo "<tr><th>Country</th><th>Caital</th></tr>";
		foreach ($countries as $x => $x_value){
			echo "<tr>";
			echo "<td>$x </td><td>$x_value</td>";
			echo "</tr>";
		}
		echo "</table>";
		echo "<br>b) Display sort array";
		foreach ($countries as $key => $value) {
			echo "<p><i>The capital of ".$key." is ".$value."</i><br></p>";
		}
	?>


	<h3><u>	Ex9</u></h3>	
	<?php
		for($i=0;$i<5;$i++){
			for($j=4-$i;$j>0;$j--){
				echo "&nbsp;&nbsp;";
			}
			for($k=0;$k<2*$i+1;$k++){
				echo "*";
			}
			echo "<br>";
		}
	?>

	<h3><u>	Ex10</u></h3>

	<form action="solve.php" method="GET">
		<span><?php echo "a)"?>&nbsp;Here is general equation: <strong>ax <sup>2</sup> + bx + c = 0</strong></span><br>	
		<span>&nbsp;&nbsp;&nbsp;&nbsp;Please input value of a, b, and c</span><br><br>
		a: <input type="number" name="a"><br><br>	
		b: <input type="number" name="b"><br><br>	
		c: <input type="number" name="c">	
		<input type="submit" value="Answer">
	</form>
	
</body>
</html>